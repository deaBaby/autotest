



val sparkVersion = "2.4.7"

//Compile / resourceGenerators += Def.task {
//  val a = new java.io.File("src/test/").listFiles.filter(_.getName.endsWith(".txt")).
//    map(x=>x.getName)
//  val file = (Compile / resourceManaged).value / "Test.txt"
//  a.foreach(x=>IO.append(file,s"$x\n"))
//  Seq(file)
//}.taskValue

lazy val root = (project in file(".")).
  settings(

    inThisBuild(List(
      organization := "TSC",
      scalaVersion := "2.12.14",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "autotest",
    unmanagedResourceDirectories in Compile += baseDirectory.value / "src/test",
    javacOptions ++= Seq("-encoding", "UTF-8"),
    assemblyOutputPath in assembly := file("lib/autotest.jar"),
    libraryDependencies ++= Seq(
      "org.apache.spark" %% "spark-core" % sparkVersion %Provided,
      "org.apache.spark" %% "spark-sql" % sparkVersion %Provided,
      "org.apache.spark" %% "spark-hive" % sparkVersion %Provided,
      "org.apache.spark" %% "spark-yarn" % sparkVersion %Provided
    )
  )

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
