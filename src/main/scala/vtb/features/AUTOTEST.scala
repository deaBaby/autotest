package vtb.features

import java.io.{File, InputStream}
import java.util.jar.JarFile

import org.apache.spark.sql.functions.{col, row_number}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.json4s.jackson.JsonMethods.parse

import scala.util.Random
import scala.util.control.NoStackTrace

object AUTOTEST {

  implicit val spark = SparkSession.
    builder().
    master("yarn").
    appName("buildsc").
    enableHiveSupport().
    getOrCreate()

  // сообщение для airflow
  var airflow_mes = ""

  //получение файла из jar, в котором находится данный класс
  val jarFile = new File(getClass.getProtectionDomain.getCodeSource.getLocation.getPath)

  //создание пустого листа, для дальнейшего добавления в него отфильтрованных имен файлов в jar
  var tests_arr: List[String] = List()

  //создание массива для передачи аргументов
  var params: Array[String] = null

  //экзэмпляр класса с доп. функционалом
  lazy val optional = new OptionalFeatures(spark, params)

  //флаг ошибки в тестах
  var flagex = false

  def main(args: Array[String]): Unit = {

    spark.sparkContext.setLogLevel("ERROR")

    params = args

    //добавление имен тестов в лист
    if (jarFile.isFile) {
      val jar = new JarFile(jarFile)
      val entries = jar.entries
      while ( {
        entries.hasMoreElements
      }) {
        val name = entries.nextElement().getName
        if (name.endsWith(".txt") || (name.endsWith(".sql")))
          tests_arr = name :: tests_arr
      }
      jar.close()
    }

    //проверка списка файлов на наличие пары source/target
    tests_arr.foreach(x=> {
      if (x.contains("source")) {
        val target_nm = x.replace("source", "target")
        if (!tests_arr.contains(target_nm)) {
          tests_arr = tests_arr.filterNot(y => y == x)
          airflow_mes += s"TARGET TEST FILE FOR $x. WAS NOT FOUND.\n"
        }
      }
      if(x.contains("target")){
        val source_nm = x.replace("target","source")
        if(!tests_arr.contains(source_nm)) {
          tests_arr = tests_arr.filterNot(y => y == x)
          airflow_mes += s"SOURCE TEST FILE FOR $x. WAS NOT FOUND.\n"
        }
      }
    }
    )

    //парсинг имён одинаковых тестов в одну строку.
    // Было:
    // test_1_source_counts.sql
    // test_1_target_counts.sql
    // test_2_source_ddl.sql
    // test_2_target_ddl.sql
    // Стало:
    // test_1_source_counts.sql,test_1_target_counts.sql
    // test_2_source_ddl.sql,test_2_target_ddl.sql
    val tests_map = tests_arr.filter(x => x.contains("source")).map(x => x + "," +
      tests_arr.filter(y => y.contains(x.replace("source", "target"))).head)

    //добавление каждого теста в лист тестов этого типа
    val counts_nm_list = tests_map.filter(x => x.contains("counts"))
    val arrays_nm_list = tests_map.filter(x => x.contains("arrays"))
    val ddl_nm_list = tests_map.filter(x => x.contains("ddl"))
    val constants_nm_list = tests_map.filter(x => x.contains("constants"))
    val other_nm_list = tests_map.filterNot(x => x.contains("counts") ||
      x.contains("arrays") || x.contains("ddl") || x.contains("constants"))

    //выполнение методов тестирования
    counts_nm_list.foreach(x => countTest(x))
    arrays_nm_list.foreach(x => arrTest(x))
    ddl_nm_list.foreach(x => ddlTest(x))
    constants_nm_list.foreach(x => constantsTest(x))
    other_nm_list.foreach(x => arrTest(x))
    //выброс ошибки в airflow
    if (flagex == true) {
      throw new Exception(airflow_mes) with NoStackTrace
    }
    spark.close()

  }
  //проверка кол-ва строк.
  def countTest(tests: String) = {

    val test_name = optional.getTestName(tests)

    //println("RUNNING A " + test_name + "...")

    val dfs = optional.getDfs(tests)
    val sourceSql = dfs(0)
    val targetSql = dfs(1)

    val sourceCount = sourceSql.head()(0).toString
    val targetCount = targetSql.head()(0).toString

    if (sourceCount == targetCount) {
      //println(s"ROW COUNT TEST HAS BEEN SUCCESSFULLY. NUMBER OF ROWS IN THE SOURCE AND TARGET - $sourceCount.")
    }
    else {
      flagex = true
      airflow_mes += s"$test_name WAS UNSUCCESSFULL:\n"
      airflow_mes += s"NUMBER OF ROWS IN THE SOURCE - $sourceCount. " +
        s"NUMBER OF ROWS IN THE TARGET - $targetCount.\n"
      // println(s"ROW COUNT TEST NOT SUCCESSFUL. NUMBER OF ROWS IN THE SOURCE - $sourceCount. " +
      //   s"NUMBER OF ROWS IN THE TARGET - $targetCount.")
    }
  }

  //проверка атрибутов
  def arrTest(tests: String): Unit = {

    val test_name = optional.getTestName(tests)

    //println("RUNNING A " + test_name + "...")

    val dfs = optional.getDfs(tests)
    val sourceSql = dfs(0).cache()
    val targetSql = dfs(1).cache()

    val colNamesSource = sourceSql.columns
    val colNamesTarget = targetSql.columns

    if (colNamesSource.length == colNamesTarget.length) {

      val sourceExcCount = sourceSql.exceptAll(targetSql).count()
      val targetExcCount = targetSql.exceptAll(sourceSql).count()

      if (sourceExcCount == 0 && targetExcCount == 0) {
        // println(s"ATTRIBUTES TEST HAS BEEN SUCCESSFULLY. SOURCE ATTRIBUTES MATCH TARGET ATTRIBUTES.")
      }
      else {
        flagex = true
        airflow_mes += s"$test_name WAS UNSUCCESSFULL:\n"
        airflow_mes += s"SOURCE ATTRIBUTES NOT MATCH TARGET ATTRIBUTES.\n"
        // println(s"ATTRIBUTES TEST NOT SUCCESSFUL. SOURCE ATTRIBUTES NOT MATCH TARGET ATTRIBUTES.")
      }
    }
    else {
      flagex = true
      airflow_mes += s"$test_name WAS UNSUCCESSFULL:\n"
      airflow_mes += s"DIFFERENT NUMBER OF COLUMNS IN THE SOURCE AND TARGET.\n"
      // println(s"ATTRIBUTES TEST NOT SUCCESSFUL. DIFFERENT NUMBER OF COLUMNS IN THE SOURCE AND TARGET")
    }
  }

  //проверка описания столбцов
  def ddlTest(tests: String): Unit = {

    var mismatch_cols = ""

    val test_name = optional.getTestName(tests)

    // println("RUNNING A " + test_name + "...")

    var ddlFlag = true

    val dfs = optional.getDfs(tests)
    val sourceSql = optional.getRes(tests.split(",").head)
    val targetSql = dfs(1)

    val sourceStructure = sourceSql.split("\n").toList
    val targetStructure = targetSql.drop(col("comment")).
      collect.
      toList
if (sourceStructure.length==targetStructure.length){
    for (i <- sourceStructure.indices) {
      if ((sourceStructure(i).split(";").head != targetStructure(i)(0).toString.toLowerCase) ||
        (sourceStructure(i).split(";").tail(0) != targetStructure(i)(1).toString.toLowerCase)) {
        ddlFlag = false
        // println("COLUMNS MISMATCH - (" + sourceStructure(i).replace(";", ",") + "|" + targetStructure(i).toString().drop(1).dropRight(1) + ")")
        mismatch_cols += "COLUMNS MISMATCH - (" + sourceStructure(i).replace(";", ",") + "|" + targetStructure(i).toString().drop(1).dropRight(1) + ")\n"
      }
    }
}
else{
  ddlFlag = false
  mismatch_cols += "DIFFERENT NUMBER OF COLUMNS IN THE SOURCE AND TARGET.\n"
}
    //
    if (ddlFlag) {
      //println(s"STRUCTURE TEST HAS BEEN SUCCESSFULLY.")
    }
    else {
      flagex = true
      airflow_mes += s"$test_name WAS UNSUCCESSFULL:\n"
      airflow_mes += mismatch_cols
      // println(s"STRUCTURE TEST NOT SUCCESSFUL.")
    }
  }

  //проверка на соответсвие константам
  def constantsTest(tests: String): Unit = {

    val test_name = optional.getTestName(tests)

    var mismatch = ""

    // println("RUNNING A " + getTestName(tests) + "...")

    var constFlag = true

    val dfs = optional.getDfs(tests)
    val targetSql = optional.getRes(tests.split(",").last)
    val sourceSql = dfs(0)

    val left = sourceSql
    val constants: List[Array[String]] = targetSql
      //.collect().map(x => x.toSeq.toList.mkString(";").toLowerCase)
      .split("\n")
      .toList
      .map(_.split(";"))
    val target =
      (
        constants.head.map(c => s"$c as ${Random.alphanumeric.take(10).mkString}") +:
          constants.tail
        )
        .map(c => s"SELECT ${c.mkString(", ")}")
    val right = target.tail.foldLeft(spark.sql(target.head))((acc, el)=> acc.union(spark.sql(el)))
    if (!compareEqualityPreciselyTo(left, right)) {
      flagex = true
      airflow_mes += s"$test_name WAS UNSUCCESSFULL:\n"
      airflow_mes += "CONSTANT VALUES DID NOT MATCH.\n"
    }

  }

  def compareEqualityPreciselyTo(left: DataFrame, right: DataFrame): Boolean = {
    try {
      left.cache()
      right.cache()

      val leftDiff  = left.exceptAll(right)
      val rightDiff = right.exceptAll(left)
      // if leftDiff and rightDiff is empty -> datasets are equal
      val isEqual = (leftDiff.count()==0) && (rightDiff.count()==0)
      isEqual
    } finally {
      left.unpersist()
      right.unpersist()
    }
  }

}


