package vtb.features

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.json4s.jackson.JsonMethods.parse

import scala.io.Source
import scala.util.Try

class OptionalFeatures(spark: SparkSession, args: Array[String]) {

  //парсинг json'а для получения параметром из airflow variables
  val parameters: Array[Map[String, Map[String, Any]]] = args match {
    case args: Array[String] if (args.isEmpty) => null
    case _ => {
      val getArgs = "{\"parameters\" : [" + args.mkString(",") + "]}"

      def jsonStrToMap(jsonStr: String): Map[String, Array[Map[String, Map[String, Any]]]] = {
        implicit val formats = org.json4s.DefaultFormats

        parse(jsonStr).extract[Map[String, Array[Map[String, Map[String, Any]]]]]
      }
      jsonStrToMap(getArgs)("parameters")
    }
  }
  //метов для получения содержмиого файлов с тестами
  def getRes(name: String): String = {
    val src: Source = Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(name))
    val container = src.getLines().mkString("\n")
    container.trim.replaceAll(";$","").trim.toLowerCase
  }

  //метов возвращающий два датафрейма после сплита названий тестов
  def getDfs(names: String): List[DataFrame] = {

    val tests_nms = names.split(",")
    val source_nm = tests_nms.head
    val target_nm = tests_nms.last

    var sourceSql = spark.emptyDataFrame
    var targetSql = spark.emptyDataFrame
    try {
      sourceSql = Try(
        spark.sql(inputParams(usageParams(source_nm),getRes(source_nm)))
      ).getOrElse(spark.sql(getRes(source_nm)))
    }
    catch {
      case ex: Throwable =>
        sourceSql = spark.emptyDataFrame
    }
    try {
      targetSql = Try(
        spark.sql(inputParams(usageParams(target_nm),getRes(target_nm)))
      ).getOrElse(spark.sql(getRes(target_nm)))
    }
    catch {
      case ex: Throwable =>
        targetSql = spark.emptyDataFrame
    }
    sourceSql :: targetSql :: Nil
  }

  //метод возвращающий название теста
  def getTestName(tests: String): String = {
    tests.split(",").head.replace("_source", "").
      replaceAll("....$", "").split("/").last.
      toUpperCase
  }

  def writeDm(tableName: String, db: String, df: DataFrame) = {
    df.repartition(1).write.mode(SaveMode.Overwrite).saveAsTable(s"$db.$tableName")
  }

  def inputParams(values: Map[String, Any], query: String): String = {
    values.foldLeft(query)((a,b) => a.replaceAllLiterally(""""$"""+b._1+""""""", b._2.toString))
  }

  def usageParams(name: String) = parameters
    .filter(_.contains(s"${getName(name)}"))
    .head(s"${getName(name)}")

  def getName(name: String) = name.split("/").last.replace(".sql", "").replace(".txt", "")
}
